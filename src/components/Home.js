import React, { Component } from 'react';
import Map from './Map';

class Home extends Component {
	render() {
	  return(
		  <Map
	   google={this.props.google}
	   center={{lat: 35.9078, lng: 127.7669}}
	   height='300px'
	   zoom={15}
	  />
		)
	}
  }

export default Home;